#pragma once
#include "Logger.h"
#include <fstream>
#include <vector>


class Keygen
{
public:
	Keygen();
	~Keygen();
	// Returns next Key a uint_8 --> "Turns all required Wheels"
	uint8_t getNextKey();
	// saves wheels-settings  to file
	bool saveDefaultSettings();
	// saves wheels-settings  to temp file
	bool saveTempSettings();
	// loads wheels-settings from file
	bool loadDefaultSettings();
	// loads wheels-settings from file
	bool loadTempSettings();

friend std::ostream& operator<< (std::ostream &os, Keygen& gen);

private:
	// saves settings  of the single rotors to a .txt file for later use
	bool saveSettings(std::string file);
	// Loads all  Wheel Settings from .bin File
	bool loadSettings(std::string file);
	// loads all needed lists of the wheels
	void init();
	// Writes vector<bool> to the given file in a compressed way (8 Bit per char)
	bool saveVector(std::vector<bool>& const vector, std::ostream& filestream);
	// Loads empty vectors into the wheels list
	void initVectors();
	//Loads in Vector from File
	bool readVector(std::vector<bool>& vector, std::istream& filestream);
	// Rotates all wheels according to the Settings by one step
	void rotateAllWheels();
	//Rotates one "Wheel" in form of a vector
	void rotateWheel(std::vector<bool>& vec);


	const std::string savefile = "wheels.bin";
	const std::string tempsavefile = "tempwheels.bin";
	std::string names[12] = { "chi_1", "chi_2", "chi_3", "chi_4", "chi_5",
					"mu_1", "mu_2", "psi_1", "psi_2", "psi_3", "psi_4", "psi_5" };
	std::vector<bool> wheels[12];

};

