#pragma once
#include <iostream>
#include "Keygen.h"
#include "ITA2_Converter.h"
#include "Logger.h" 

class Encryptor
{
public:
	Encryptor(Keygen* keygen);
	~Encryptor();
	void encrypt(std::istream& in, std::ostream& out);
	void decrypt(std::istream& in, std::ostream& out);


private:
	Keygen* keygen = nullptr;
	ITA2_Converter converter;
};

