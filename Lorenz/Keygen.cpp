#include "Keygen.h"

Keygen::Keygen()
{
	init();
	INF("New Keygenerator succesfully generated");
}
Keygen::~Keygen()
{
	if (saveDefaultSettings()) {
		ERR("Could not save Settings while closing");
	}
	else {
		INF("Saved and exiting");
	}
}


// loads all needed lists of the wheels
void Keygen::init()
{
	initVectors();
	loadDefaultSettings();
}

// Returns next Key a uint_8 --> "Turns all required Wheels"
uint8_t Keygen::getNextKey()
{
	uint8_t bit1 = (wheels[0].at(0) ^ wheels[7].at(0)); 
	uint8_t bit2 = ((wheels[1].at(0) ^ wheels[8].at(0)) << 1);
	uint8_t bit3 = ((wheels[2].at(0) ^ wheels[9].at(0)) << 2);
	uint8_t bit4 = ((wheels[3].at(0) ^ wheels[10].at(0)) << 3);
	uint8_t bit5 = ((wheels[4].at(0) ^ wheels[11].at(0)) << 4);

	DBG(std::to_string((int) bit1 << (int)bit2 << (int)bit3 << (int)bit4 << (int)bit5));

	uint8_t res = bit1 | bit2 | bit3 | bit4 | bit5;

	rotateAllWheels();
	return res;
}

//returns Keygens Wheel to ostream
std::ostream& operator<< (std::ostream &os, Keygen& gen) {
	std::cout << "wheels\t0         1         2         3         4         5         6\n";
	std::string* name = gen.names;
	for (std::vector<bool>* wheel = gen.wheels; wheel != &gen.wheels[12]; wheel++) {
		os << *name << "\t";
		name++;
		for (std::vector<bool>::iterator it = wheel->begin(); it != wheel->end(); it++) {
			os << *it;
		}
		os << std::endl;
	}
	return os;
}

// saves settings  of the single rotors to a .txt file for later use
bool Keygen::saveSettings(std::string place)
{
	std::ofstream file(place, std::ios::binary);
	if (file.is_open())
	{
		for (int i = 0; i < 12; i++) {
			saveVector(wheels[i], file);
		}
		file.close();
		INF("Successfully saved wheel settings to bin");
		return true;
	}
	else {
		ERR("could not open File");
		return false;
	}
}
// Writes vector<bool> to the given file in a compressed way (8 Bit per char)
bool Keygen::saveVector(std::vector<bool>& const vector, std::ostream& filestream)
{
	unsigned int const size = vector.size();
	std::streamsize charsize = size / 8; //number of bytes needed to save the whole vector
	if (size % 8 != 0) ++charsize;
	char * toWrite = new char[charsize]; //char[] to collect all bool-values
	int i = 0;
	unsigned char ds = 0;

	for (std::vector<bool>::iterator it = vector.begin(); it != vector.end(); it++) {
		unsigned char a = *it; //value of vector
		ds = ds | (a << (7 - i % 8)); // shifting a into the correct position and OR-ing it to the value
		if (i % 8 == 7) {
			toWrite[i / 8] = ds;
			ds = 0; //make a new byte to write to
		}
		i++;
	}

	if (i % 8 != 0) toWrite[i / 8] = ds; //sould there still be an unwritten byte add it to char[]
	if (!filestream.write(toWrite, charsize)) {
		ERR("Cant write to file");
		return false;
	}

	delete[] toWrite;
	return true;
}

// Loads empty vectors into the wheels list
void Keygen::initVectors()
{
	std::vector<bool> chi1(43, false);
	std::vector<bool> chi2(47, false);
	std::vector<bool> chi3(51, false);
	std::vector<bool> chi4(53, false);
	std::vector<bool> chi5(59, false);
	std::vector<bool> mu1(37, false);
	std::vector<bool> mu2(61, false);
	std::vector<bool> psi1(41, false);
	std::vector<bool> psi2(31, false);
	std::vector<bool> psi3(29, false);
	std::vector<bool> psi4(26, false);
	std::vector<bool> psi5(23, false);

	wheels[0] = chi1;
	wheels[1] = chi2;
	wheels[2] = chi3;
	wheels[3] = chi4;
	wheels[4] = chi5;
	wheels[5] = mu1;
	wheels[6] = mu2;
	wheels[7] = psi1;
	wheels[8] = psi2;
	wheels[9] = psi3;
	wheels[10] = psi4;
	wheels[11] = psi5;
}
// //Loads in Vector from File
bool Keygen::readVector(std::vector<bool>& vector, std::istream& filestream)
{
	int const size = vector.size();
	std::streamsize charsize = size / 8; //number of bytes needed to save the whole vector
	if (size % 8 != 0) ++charsize;
	char * read = new char[charsize]; //char[] to collect all bool-values
	if(!filestream.read(read, charsize))ERR("Error reading File"); //read in Vector
	int vector_index = 0;

	for (int i = 0; i < charsize; i++) {
		uint8_t data = read[i]; // convert byte by byte
		for (int pos = 0; pos < 8; pos++) {
			uint8_t mask = 1 << (7-pos); //shift mask in correct position
			bool stuff = data & mask;
			if(vector_index < size) vector.at(vector_index) = stuff;
			++vector_index;
		}
	}
	delete[] read;
	return false;
}
// Loads all  Wheel Settings from .bin File
bool Keygen::loadSettings(std::string place)
{
	std::ifstream file(place, std::ios::binary);
	if (file.is_open()) {
		for (int i = 0; i < 12; i++) {
			readVector(wheels[i], file);
		}
		file.close();
		return true;
	}
	else {
		ERR("Can't read from .bin, using default Settings");
		return false;
	}
}

// Rotates all wheels according to the Settings by one step
void Keygen::rotateAllWheels() {
	bool rotateSecondMu = wheels[6].at(0);
	bool rotateSecondRow = wheels[7].at(0);
	for (int i = 0; i < 6; i++) {
		rotateWheel(wheels[i]);
	}
	if (rotateSecondMu) {
		rotateWheel(wheels[7]);
	}
	if (rotateSecondMu && rotateSecondRow) {
		for (int i = 7; i < 12; i++) {
			rotateWheel(wheels[i]);
		}
	}
}
//Rotates one "Wheel" in form of a vector
void Keygen::rotateWheel(std::vector<bool>& vec)
{
	bool temp = vec.at(0);
	vec.erase(vec.begin());
	vec.push_back(temp);
}

// saves wheels-settings  to file
bool Keygen::saveDefaultSettings()
{
	bool succ = saveSettings(savefile);
	if (succ) {
		INF("Saved to default settings");
	}
	else {
		ERR("Error saving to default settings");
	}
	return succ;
}
// saves wheels-settings  to temp file
bool Keygen::saveTempSettings()
{
	bool succ = saveSettings(tempsavefile);
	if (succ) {
		INF("Saved to temp settings");
	}
	else {
		ERR("Error saving to temp settings");
	}
	return succ;
}
// loads wheels-settings from file
bool Keygen::loadDefaultSettings()
{
	bool succ = loadSettings(savefile);
	if (succ) {
		INF("Loaded from default settings");
	}
	else {
		ERR("Error loading from default settings");
	}
	return succ;
}
// loads wheels-settings from file
bool Keygen::loadTempSettings()
{
	bool succ = loadSettings(tempsavefile);
	if (succ) {
		INF("Loaded from temp settings");
	}
	else {
		ERR("Error loading from temp settings");
	}
	return succ;
}

