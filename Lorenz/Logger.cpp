#include "Logger.h"

namespace Logger
{
	const std::string STRING_ERROR = "[ERROR]";
	const std::string STRING_DEBUG = "[DEBUG]";
	const std::string STRING_INFO = "[INFO]";
	int counter = 0;

	//prints Logger info to Stream defined in header File
	void print(const std::string& pre, const std::string& in, const std::string& place);

	//prints Error message to Logger
	void error(const std::string& in, const std::string& classname)
	{
		print(STRING_ERROR, in, classname);
	}

	//prints Debug message to Logger
	void debug(const std::string& in, const std::string& classname)
	{
		if (LOGGINGLEVEL >= 2) {
			print(STRING_DEBUG, in, classname);
		}
	}

	//prints Info message to Logger

	void info(const std::string& in, const std::string& classname)
	{	
		if (LOGGINGLEVEL >= 1) {
			print(STRING_INFO, in, classname);
		}	
	}
	
	//Returns current Date and Time in given Format
	const std::string currentDateTime() {
		char buffer[26];
		time_t rawtime;
		struct tm timeinfo;
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", &timeinfo);
		return buffer;
	}

	void print(const std::string& pre, const std::string& in, const std::string& place)
	{
		if (EFFICIENT) {
			std::cout << pre << "\t" <<	counter++ << '\t' <<  place << '\t' << in << std::endl;
		}
		else {
			std::string classname = place;
			classname.erase(0, 6);
			int length = (int) classname.length();
			classname.erase(length - 10, 10);
			if (length < 20) {
				classname.append("     ");
			}
			std::cout << pre << "\t  [" << currentDateTime() << "] - " <<
				counter++ << '\t' << "Class: " << classname << '\t' << in << std::endl;
		}
	}
}




