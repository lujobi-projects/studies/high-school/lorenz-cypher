#include "ITA2_Converter.h"


ITA2_Converter::ITA2_Converter()
{
	loadResource();
	INF("New ITA2_Converter successfully generated");
}


ITA2_Converter::~ITA2_Converter()
{
}


// Loads ITA2-Table into Map
void ITA2_Converter::loadResource(){
	bool success=
		insert(0b00000000, '/') &&
		insert(0b00000001, 'T') &&
		insert(0b00000010, '3') &&
		insert(0b00000011, 'O') &&
		insert(0b00000100, '9') &&
		insert(0b00000101, 'H') &&
		insert(0b00000110, 'N') &&
		insert(0b00000111, 'M') &&
		insert(0b00001000, '4') &&
		insert(0b00001001, 'L') &&
		insert(0b00001010, 'R') &&
		insert(0b00001011, 'G') &&
		insert(0b00001100, 'I') &&
		insert(0b00001101, 'P') &&
		insert(0b00001110, 'C') &&
		insert(0b00001111, 'V') &&
		insert(0b00010000, 'E') &&
		insert(0b00010001, 'Z') &&
		insert(0b00010010, 'D') &&
		insert(0b00010011, 'B') &&
		insert(0b00010100, 'S') &&
		insert(0b00010101, 'Y') &&
		insert(0b00010110, 'F') &&
		insert(0b00010111, 'X') &&
		insert(0b00011000, 'A') &&
		insert(0b00011001, 'W') &&
		insert(0b00011010, 'J') &&
		insert(0b00011011, '5') &&
		insert(0b00011100, 'U') &&
		insert(0b00011101, 'Q') &&
		insert(0b00011110, 'K') &&
		insert(0b00011111, '8');
	if (success) {
		DBG("Recource successfully loaded");
	}
	else {
		ERR("Error loading ITA2_Table");
	}
}


// Puts value in both maps--> bidirectional map
bool ITA2_Converter::insert(uint8_t signal, char c)
{
	std::pair<std::map<uint8_t, char>::iterator, bool> ret1;
	std::pair<std::map<char, uint8_t>::iterator, bool> ret2;
	ret1 = dec.insert(std::make_pair(signal, c));
	ret2 = enc.insert(std::make_pair(c, signal));
	bool res = ret1.second && ret2.second;
	if (res == false) {
		std::string str = "Error inserting Element ";
		ERR(str.append(std::to_string((int)signal)).append( " , ").append(1u, c));
	}
	return res;
}


// converts Signal to corresponding char value
char ITA2_Converter::convert(uint8_t signal)
{
	std::map<uint8_t, char>::iterator it = dec.find(signal);
	if (it != dec.end()) {
		return it->second;
	}
	else {
		ERR("No value found to given key in dec-table");
		return NULL;
	}
}


// converts char to corresponding signal value
uint8_t ITA2_Converter::convert(char c)
{
	if (c == ' ') c = '9';
	std::map<char, uint8_t>::iterator it = enc.find(c);
	if (it != enc.end()) {
		return it->second;
	}
	else {
		ERR("No value found to given key in enc-table");
		return NULL;
	}
}
