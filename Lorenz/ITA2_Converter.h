#pragma once

#include <map>
#include <cstdint>
#include "Logger.h"

//#define LEVELDEBUG
class ITA2_Converter
{
public:
	ITA2_Converter();
	~ITA2_Converter();

	// converts Signal to corresponding char value
	char convert(uint8_t signal);
	// converts char to corresponding signal value
	uint8_t convert(char c);

private:
	// Loads ITA2-Table into Map
	void loadResource();
	// Puts value in both maps--> bidirectional map
	bool insert(uint8_t signal, char c);


	std::map<uint8_t, char> dec;
	std::map<char, uint8_t> enc;
};

