#include "Encryptor.h"

Encryptor::Encryptor(Keygen* gen)
{
	keygen = gen;
	INF("New Encryptor successfully generated");
}


Encryptor::~Encryptor()
{ 
}


std::ostream& operator<< (std::ostream& os, uint8_t& signal) {
	os  << ((bool)((1 << 4)&signal))
		<< ((bool)((1 << 3)&signal)) 
		<< ((bool)((1 << 2)&signal))
		<< ((bool)((1 << 1)&signal))
		<< ((bool)(1&signal));
	 return os;
}


void Encryptor::encrypt(std::istream& in, std::ostream& out)
{

	INF("Entering encryption-mode");
	char c = 0;
	while (true) {
		in >> c;
		if (c == '/') { //check for escaping character
			INF("Leaving encryption-mode");
			break;
		}
		if (in.peek() == '\n') out << std::endl; //keep the newline
		c = toupper(c); 
		DBG(std::to_string(c));
		uint8_t sig = converter.convert(c);
		uint8_t key = keygen->getNextKey();
		uint8_t crib = key ^ sig; //XOR key to signal
		out << converter.convert(crib) << " ";
	}
}


void Encryptor::decrypt(std::istream& in, std::ostream& out)
{ 
	INF("Entering decryption-mode");
	char c = 0;
	while (true) {
		if (in.peek() == '/') { //check for escape 
			in >> c;
			INF("Leaving decryption-mode");
			break;
		}
		if (in.peek() == ' ') in >> c; //remove whitespace between two signals
		if (in.peek() == '\n)') out << std::endl; // keep the newline
		char signal[5]; //size of the signal input
		in >> signal;
		uint8_t sig = 0; //signal
		bool ok = true; //keep track wether it is a correct signal to be put out
		for (int i = 0; i < 5; ++i) {
			if (signal[i] != '1' && signal[i] != '0') { //check for correct signal
				ERR("Inexpected input only 0's and 1's allowed");
				ok = false; //prevet incorrect output
				break; //stopfor this input
			}
			sig += ((bool)(signal[i] == '1')) << (4 - i); //add correctli shifted bit to the signal uint
		}
		uint8_t key = keygen->getNextKey();
		uint8_t crib = key ^ sig;
		if(ok) out <<  crib; //if ok add signal to output stream
	}
}


