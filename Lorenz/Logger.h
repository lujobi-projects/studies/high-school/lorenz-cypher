#pragma once
#include <time.h>
#include <string>
#include <iostream>
#include <typeinfo>


#ifdef LEVELERROR
#define LOGGINGLEVEL 0

#elif defined LEVELDEBUG
#define LOGGINGLEVEL 2

#else
#define LOGGINGLEVEL 1
#endif

#ifdef LOGGINGLIGHT
#undef EFFICIENT
#define EFFICIENT 1
#else
#undef EFFICIENT
#define EFFICIENT 0
#endif // LOGGINGLIGHT


#define ERR(message) Logger::error(message, typeid(this).name())
#define INF(message) Logger::info(message, typeid(this).name())
#define DBG(message) Logger::debug(message, typeid(this).name())


namespace Logger
{
	//prints Debug essage to Logger
	void debug(const std::string& in, const std::string& classname);
	//prints Info message to Logger
	void info(const std::string& in, const std::string& classname);
	//prints Error message to Logger
	void error(const std::string& in, const std::string& classname);
};

