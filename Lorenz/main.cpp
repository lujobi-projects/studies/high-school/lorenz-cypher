#pragma once
#include "Encryptor.h"
#include "ITA2_Converter.h"
#include <iostream>

std::istream &in = std::cin;
std::ostream &out = std::cout;
	
int main() {
	Keygen keygen;
	Encryptor en = Encryptor(&keygen);

	std::string command = "";
	while (command != "exit") {
		if (in.peek() == '/') {
			char c;
			in >> c;
		}
		in >> command;
		if (command == "enc") {
			en.encrypt(in, out);
		}
		else if (command == "dec") {
			en.decrypt(in, out);
		}
		else if (command == "show") {
			out<<keygen;
		}
		else if (command == "save") {
			keygen.saveDefaultSettings();
		}
		else if (command == "savetemp") {
			keygen.saveTempSettings();
		}
		else if (command == "load") {
			keygen.loadDefaultSettings();
		}
		else if (command == "loadtemp") {
			keygen.loadTempSettings();
		}
		else {
			out << "No such command found";
		}
	}

	return 0;
}